# Learning NES 6502 Assembly

This repository contains all the useful stuff that I may need while learning 6502 Assembly.  
I'm currently following a series of tutorials named Nerdy Nights, made by Roth at [this forum](http://nintendoage.com/forum/messageview.cfm?catid=22&threadid=7155).

## Repository content

### FCEUXD executable

FCEUXD is an open-source Nintendo Entertainment System and Family Computer Disk System emulator, available [here](http://www.fceux.com/web/download.html).
FCEUXD offers tools for debugging, rom-hacking, map making, Tool-assisted movies, and Lua scripting. This will be the emulator that I'm going to use to learn and develop NES games.

### Tools

This folder contains every tools that I use while learning Assembly 6502. 

At the moment, this folder contains:
*  NESASM: The assembler needed to build Assembly 6502 code into .nes files. [Download here](http://www.nespowerpak.com/nesasm/NESASM3.zip);
*  YY-Chr: YY-CHR is a graphic editing program, which can edit bitplane formats of most of the consoles. [Download here](https://www.romhacking.net/utilities/119/);
*  makechr: makechr is a tool for generating NES graphics. Its primary operation is to take pixel art images and split it into components: chr, nametable, palette, attributes, and spritelist. [Download here](https://github.com/dustmop/makechr).

### Tutorial

This folder contains all of the tutorials projects that I'm currently following.  
At the moment, I'm working on making a version of Pong that is playing on real NES hardware.
The ball physics, the scoring system, the paddle movement and hit detection are all currently working.

### Task List

The repo for this task list is found [here](https://gitlab.com/amonzerol/nes/tree/master/Tutorial/pong1):
- [x] Making the ball and the paddles sprites in YY-CHR;
- [x] Correctly displaying the paddles sprites;
- [x] First player paddle movements;
- [x] Make an always losing AI (his paddle will always be over the ball!);
- [x] Displaying the ball on the screen;
- [x] Ball movements;
- [x] Ball collision with the four walls
- [x] Ball collision with the paddles;
- [x] Modifying the nametable 1 to contain all english characters and numbers;
- [x] Displaying single digit scores (0-9);
- [x] Displaying three digits scores (0-999)
- [x] Displaying a title screen;
- [x] Erasing the title screen upon entering the game;
- [ ] Displaying an end screen after one of the two scores hits 10;
- [ ] On the end screen, display the winning player;
- [ ] Testing the collision offset with the ball and paddle #1;
- [ ] Offering to play versus an always winning AI, an always losing AI or an IRL second player;
- [ ] Displaying a timer;
- [ ] Producing a sound when the ball hits a paddle;
- [ ] Beautify everything (this may not happen in this project, I still have lots of practical stuff to learn before moving on to the artistic side);
- [ ] Putting this project into the archives;
- [ ] Moving on to another game !


### Screenshot of my pong project:  
![Pong](https://i.imgur.com/hChslRn.png)

#### Update #1

I added a titlescreen to my Pong game ! When you press the down key, the game starts.
Now, I need to figure out how so erase the whole background when I'm changing gamestates.

![Imgur](https://i.imgur.com/yjOldo7.png)

#### Update #2

The background now erases itself when you enter the game (down key on the D-Pad).
Previously, when I tried to redraw the whole background during the NMI, the VBlank time
was too short to draw the whole nametable. The result of this was partially erased background.

![Imgur](https://i.imgur.com/CEibPGB.png)

The solution to this was to turn the rendering off during the NMI to do it all at once.
It can be done by turning off the bits 3 and 4 from the register $2001.

```
LDA #%00000110  ; no sprites, no background, no clipping on left side  
STA $2001 
```


